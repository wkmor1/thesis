#!/bin/bash

# get_ch () {
#   sed '1{/^---$/!q};1,/^---$/d' ../$1/$1.Rmd > $1.Rmd
#   sed -i "1s;^;# $2\n;" $1.Rmd
# }
# 
# get_ch voiReview   'Valuing information for conservation and natural resource management: a review'
# get_ch voiWoodland 'The value of information for woodland management: updating a state and transition model'
# get_ch voiConsPlan 'The value of information for spatial conservation planning'
# get_ch voiConsAuc  'The value of information for conservation auctions'
# get_ch voiRisk     'The value of information to the risk-averse conservation decision maker'
# 
# cat ../voiReview/voiReview.bib      > thesis.bib
# cat ../voiWoodland/voiWoodland.bib >> thesis.bib
# cat ../voiConsPlan/voiConsPlan.bib >> thesis.bib
# cat ../voiConsAuc/voiConsAuc.bib   >> thesis.bib
# cat ../voiRisk/voiRisk.bib         >> thesis.bib
# 
# #appendices
# cp ../voiReview/appendix.tex appendix2.tex
# cp ../voiConsAuc/appendix1.tex appendix3.tex
# cp ../voiConsAuc/appendix2.tex appendix4.tex
# cp -R ../voiConsAuc/figure figure
