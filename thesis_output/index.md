---
documentclass: book
papersize: a4paper
bibliography: thesis.bib
biblio-style: thesis.bst
toc-depth: 3
lof: true
lot: true
subparagraph: true
geometry: margin=3cm
---


